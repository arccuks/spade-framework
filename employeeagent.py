import time
import random
import json
from spade.agent import Agent
from spade.message import Message
from spade.behaviour import FSMBehaviour, State
from States import EmployeeState
from Actions import TaskActions

MANAGER = "gamemaster@jix.im"

class EmployeeBehaviour(FSMBehaviour):
    async def on_start(self):
        print(f"Starting state {self.current_state}")

    async def on_end(self):
        print(f"End state {self.current_state}")
        

class EmployeeAgent(Agent):
    async def setup(self):
        employeeBehaviour = EmployeeBehaviour()
        employeeBehaviour.add_state(name=EmployeeState.WAIT_TASK, state=WaitTask(), initial=True)
        
        employeeBehaviour.add_state(name=EmployeeState.AT_TABLE, state=AtTable())
        employeeBehaviour.add_state(name=EmployeeState.SHUFFLE_CARDS, state=ShuffleCards())
        employeeBehaviour.add_state(name=EmployeeState.LEAD_GAME, state=LeadGame())
        
        employeeBehaviour.add_transition(source=EmployeeState.WAIT_TASK, dest=EmployeeState.WAIT_TASK)
        employeeBehaviour.add_transition(source=EmployeeState.WAIT_TASK, dest=EmployeeState.AT_TABLE)

        employeeBehaviour.add_transition(source=EmployeeState.AT_TABLE, dest=EmployeeState.AT_TABLE)
        employeeBehaviour.add_transition(source=EmployeeState.AT_TABLE, dest=EmployeeState.SHUFFLE_CARDS)
        employeeBehaviour.add_transition(source=EmployeeState.AT_TABLE, dest=EmployeeState.LEAD_GAME)

        employeeBehaviour.add_transition(source=EmployeeState.SHUFFLE_CARDS, dest=EmployeeState.WAIT_TASK)

        employeeBehaviour.add_transition(source=EmployeeState.LEAD_GAME, dest=EmployeeState.WAIT_TASK)
        
        self.add_behaviour(employeeBehaviour)

class WaitTask(State):
    async def run(self):


        message = Message(to=MANAGER)
        message.body = json.dumps({"state": EmployeeState.WAIT_TASK.value})
        print(f"Send: {message.body}")
        await self.send(message)

        responseMessage = await self.receive(timeout=7)
        
        if responseMessage:
            print(f"Response: {responseMessage.body}")
            body = json.loads(responseMessage.body)

            if body["action"] == TaskActions.GO_TO_TABLE.value:
                self.set_next_state(EmployeeState.AT_TABLE)
        else:
            self.set_next_state(EmployeeState.WAIT_TASK)

class ShuffleCards(State):
    async def run(self):
        
        makeMistake = random.randrange(100) > 90
        message = Message(to=MANAGER)

        if makeMistake:
            message.body = json.dumps({"state": EmployeeState.MADE_MISTAKE.value})
            print(f"Send: {message.body}")
            await self.send(message)
        else:
            message.body = json.dumps({"state": EmployeeState.CARDS_SHUFFLED.value})
            print(f"Send: {message.body}")
            await self.send(message)

        responseMessage = await self.receive(timeout=7)
        print(f"Response: {responseMessage.body}")

        self.set_next_state(EmployeeState.WAIT_TASK)

class AtTable(State):
    async def run(self):
        # print("at table...")
        message = Message(to=MANAGER)
        message.body = json.dumps({"state": EmployeeState.AT_TABLE.value})
        print(f"Send: {message.body}")
        await self.send(message)

        responseMessage = await self.receive(timeout=7)
        if responseMessage:
            print(f"Response: {responseMessage.body}")
            body = json.loads(responseMessage.body)

            if body["action"] == TaskActions.LEAD_GAME.value:
                self.set_next_state(EmployeeState.LEAD_GAME)
            
            if body["action"] == TaskActions.SUFFLE_CARDS.value:
                self.set_next_state(EmployeeState.SHUFFLE_CARDS)
            
            if body["action"] == TaskActions.GO_TO_TABLE.value:
                self.set_next_state(EmployeeState.AT_TABLE)
        else:
            self.set_next_state(EmployeeState.AT_TABLE)



class LeadGame(State):
    async def run(self):
        # print("leading game...")
        makeMistake = random.randrange(100) > 90
        message = Message(to=MANAGER)

        if makeMistake:
            message.body = json.dumps({"state": EmployeeState.MADE_MISTAKE.value})
            print(f"Send: {message.body}")
            await self.send(message)
        else :
            message.body = json.dumps({"state": EmployeeState.GAME_FINISHED.value})
            print(f"Send: {message.body}")
            await self.send(message)

        responseMessage = await self.receive(timeout=7)
        print(f"Response: {responseMessage.body}")

        self.set_next_state(EmployeeState.WAIT_TASK)

if __name__ == "__main__":
    agent = EmployeeAgent("employee1@jix.im", "employee1")
    agent.start()

    while True:
        try:
            time.sleep(1)
        except KeyboardInterrupt:
            break

    agent.stop()