import time
import random
import asyncio
import uuid
import json
from spade.agent import Agent
from spade.behaviour import CyclicBehaviour, PeriodicBehaviour, OneShotBehaviour, FSMBehaviour, State
from spade.message import Message
from States import EmployeeState
from Actions import TaskActions


tasks = []

class GameMasterAgent(Agent):

    class CheckForSuffleBehaviour(PeriodicBehaviour):
        async def run(self):
            randomTask = random.randrange(100)

            if randomTask > 10:
                return

            if len(tasks) > 5:
                return
            
            tasks.append({"id": f"{uuid.uuid4()}", "action": TaskActions.SUFFLE_CARDS.value})
            print(tasks)
        
        async def on_end(self):
            await self.agent.stop()

    class CheckForNewGameBehaviour(PeriodicBehaviour):
        async def run(self):
            randomTask = random.randrange(100)

            if randomTask > 15:
                return

            if len(tasks) > 5:
                return
            
            tasks.append({"id": f"{uuid.uuid4()}", "action": TaskActions.LEAD_GAME.value})
            print(tasks)

        async def on_end(self):
            await self.agent.stop()
            

    class CheckForMessageBehaviour(PeriodicBehaviour):
        async def run(self):
            message = await self.receive(timeout=7) 
            
            if (not message):
                return

            messageContent = json.loads(message.body)
                
            await self.give_task(message, messageContent["state"], tasks)
                    

        async def give_task(self, message, state, tasks):
            
            reply = message.make_reply()

            if (len(tasks) == 0 
                or state == EmployeeState.MADE_MISTAKE.value
                or state == EmployeeState.CARDS_SHUFFLED.value
                or state == EmployeeState.GAME_FINISHED.value):
                reply.body = json.dumps({"action": TaskActions.WAIT_FOR_TASK.value})
                await self.send(reply)
                return

            if state == EmployeeState.AT_TABLE.value: 

                taskIndex = random.randrange(len(tasks))
                randomTask = tasks[taskIndex]
                tasks.pop(taskIndex - 1)

                reply.body = json.dumps(randomTask)
                await self.send(reply)
                return

            else:
                reply.body = json.dumps({"action": TaskActions.GO_TO_TABLE.value})
                await self.send(reply)
            
    async def setup(self):
        b = self.CheckForSuffleBehaviour(period=1)
        self.add_behaviour(b)

        b = self.CheckForNewGameBehaviour(period=1)
        self.add_behaviour(b)

        b = self.CheckForMessageBehaviour(period=1)
        self.add_behaviour(b)

if __name__ == "__main__":
    agent = GameMasterAgent("gamemaster@jix.im", "gamemaster")
    agent.start()

    while True:
        time.sleep(1)