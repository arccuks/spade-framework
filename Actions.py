from enum import Enum

class TaskActions(Enum):
    GO_TO_TABLE = "GO_TO_TABLE"
    LEAD_GAME = "LEAD_GAME"
    SUFFLE_CARDS = "SUFFLE_CARDS"
    WAIT_FOR_TASK = "WAIT_FOR_TASK"
